//
//  AuthStatus.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-23.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation

struct authStatus: Codable {
    let status: Bool
}
