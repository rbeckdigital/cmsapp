//
//  Post.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-07.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation

struct Post: Codable {
    var title: String
    var body: String
    var id: Int?
    var author: String?
    var url: String?
    var tag: String?
    var createdAt: String?
    var updatedAt: String?
    
}


