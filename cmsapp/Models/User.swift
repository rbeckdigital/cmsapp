//
//  User.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-11-30.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit

struct User: Codable {
    let email: String

}
