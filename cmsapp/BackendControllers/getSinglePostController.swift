//
//  getSinglePostController.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-19.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation


class PostRemoteController {
    
    func get(_ postId:Int) -> Post? {
        
        var fetchedPost:Post?
        
        let postEP: String = global.appUrlDomain+"post/object/\(postId)"
        
        guard let epURL = URL(string: postEP) else {
            print("Error: cannot create URL")
            return nil
        }
        
        var epUrlRequest = URLRequest(url: epURL)
        
        epUrlRequest.httpMethod = "GET"
        epUrlRequest.allHTTPHeaderFields = ["Content-Type":"application/json"]
        
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: epUrlRequest)  {
            (data, response, error)  in
            guard error == nil else {
                print("error calling GET on /post")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            
            
            let decoder = JSONDecoder()
            
            do {
                let post = try decoder.decode(Post.self, from: responseData)
                fetchedPost = post
                
            } catch {
                print("error trying to convert data to JSON")
                print(error)
            }
            
        }
        task.resume()
        
        while fetchedPost?.id == nil {
            
        }
        
        return fetchedPost
    }
    
}
