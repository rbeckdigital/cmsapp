//
//  postPostRemoteController.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-24.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation


class postPostRemoteController {
    
    func post(post:Post, action:Action) -> Bool? {
        
        var endpoint: String = global.appUrlDomain+"post/\(post.id)"
        
        if action == .post {endpoint = global.appUrlDomain+"/post/"}
        
        
        guard let actionURL = URL(string: endpoint) else {
            print("Error: cannot create URL")
            return nil
        }
        
        var urlRequest = URLRequest(url: actionURL)
        
        switch action {
            case .post: urlRequest.httpMethod = "POST"
            case .patch: urlRequest.httpMethod = "PATCH"
        }
        
        urlRequest.allHTTPHeaderFields = ["Content-Type":"application/json"]

        do {

            let jsonData = try JSONEncoder().encode(post)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            urlRequest.httpBody = jsonData
            
        } catch {
            print("Error: cannot create JSON from Provided Data")
            return nil
        }
        
        let session = URLSession.shared

        let task = session.dataTask(with: urlRequest)  {
            (data, response, error)  in
            guard error == nil else {
                print("error calling POST on /post")
                print(error!)
                return
            }
            guard let _ = data else {
                print("Error: did not receive data")
                return
            }
            
            
            
        }
        task.resume()
        
        
        return true
    }
    
}


enum Action {
    case post
    case patch
}


struct backendError {
    var error:String
    var reason:String
}
