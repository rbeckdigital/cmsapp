//
//  postListContoller.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-07.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation

class postListRemoteController {
    
    func get() -> [Post] {
        
        var postList = [Post]()
        
        let postListEP: String = global.appUrlDomain+"post"
        guard let epURL = URL(string: postListEP) else {
            print("Error: cannot create URL")
            return []
        }
        
        var epUrlRequest = URLRequest(url: epURL)
        
        epUrlRequest.httpMethod = "GET"
        epUrlRequest.allHTTPHeaderFields = ["Content-Type":"application/json"]
        
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: epUrlRequest)  {
            (data, response, error)  in
            guard error == nil else {
                print("error calling GET on /post")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides

                    
                    let decoder = JSONDecoder()
                    
                    do {
                        let post = try decoder.decode([Post].self, from: responseData)
                        postList = post
                    } catch {
                        print("error trying to convert data to JSON")
                        print(error)
                    }
            
        }
        task.resume()
        
        while postList.count == 0 {
            
        }
        
        return postList
    }
    
}
