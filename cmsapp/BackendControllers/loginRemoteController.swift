//
//  backendControllers.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-11-30.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation


class loginRemoteController {
    
    func login(email:String, password:String) -> Int? {
        
        var userId: Int? = 0
        
        let loginEndpoint: String = global.appUrlDomain+"login"
        guard let loginURL = URL(string: loginEndpoint) else {
            print("Error: cannot create URL")
            return nil
        }
        
        var loginUrlRequest = URLRequest(url: loginURL)
        
        loginUrlRequest.httpMethod = "POST"
        loginUrlRequest.allHTTPHeaderFields = ["Content-Type":"application/json"]
        let loginReq: [String: Any] = ["email": email, "password": password]
        let jsonLogin: Data
        do {
            jsonLogin = try JSONSerialization.data(withJSONObject: loginReq, options: [])
            loginUrlRequest.httpBody = jsonLogin
        } catch {
            print("Error: cannot create JSON from Login")
            return nil
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: loginUrlRequest)  {
            (data, response, error)  in
            guard error == nil else {
                print("error calling POST on /login")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            print(responseData)
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let receivedUser = try JSONSerialization.jsonObject(with: responseData,
                                                                          options: []) as? [String: Any] else {
                                                                            print("Could not get JSON from responseData as dictionary")
                                                                            return
                }
                print("The User is: \(receivedUser)"  )
    
                userId = receivedUser["id"] as? Int
                
                
                
            } catch  {
                print("error parsing response from POST on /login")
                return
            }
        }
        task.resume()
        
        while userId == 0 {
            
        }
        
        return userId
    }
    
}
