//
//  getAuthStatusController.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-23.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation

class AuthstatusRemoteController {
    
    func get() -> authStatus? {
        
        var authenticationStatus: authStatus?
        
        let callurl: String = global.appUrlDomain+"authStatus"
        
        guard let epURL = URL(string: callurl) else {
            print("Error: cannot create URL")
            return nil
        }
        
        var epUrlRequest = URLRequest(url: epURL)
        
        epUrlRequest.httpMethod = "GET"
        epUrlRequest.allHTTPHeaderFields = ["Content-Type":"application/json"]
        
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: epUrlRequest)  {
            (data, response, error)  in
            guard error == nil else {
                print("error calling URL")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            // parse the result as JSON, since that's what the API provides
            
            
            let decoder = JSONDecoder()
            
            do {
             
           
                let status = try decoder.decode(authStatus.self, from: responseData)
                authenticationStatus = status
                
            } catch {
                print("error trying to convert data to JSON")
                print(error)
            }
            
        }
        task.resume()
        
        while authenticationStatus?.status == nil {
            
        }
        
        return authenticationStatus
    }
    
}
