//
//  postViewController.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-19.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit

class postVC:UITableViewController  {
    
    var postId:Int?
    var post:Post?
    var action:Action?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if postId != nil {
            // get post from backend
            let pc = PostRemoteController()
            post = pc.get(postId!)
            action = Action.post
            
        } else {
            post = Post(title: "", body: "", id: nil, author: nil, url: nil, tag: nil, createdAt: nil, updatedAt: nil)
            action = Action.post
        }
        
        print(AuthstatusRemoteController().get())
        
        if AuthstatusRemoteController().get()?.status == false {
            performSegue(withIdentifier: "loginSegue", sender: nil)
        }
        
        
        
        
    }
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func loginButtonPress(_ sender: Any) {
        performSegue(withIdentifier: "loginSegue", sender: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if AuthstatusRemoteController().get()?.status == false {
            loginButton.isHidden = false
        } else {
            loginButton.isHidden = true
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(91.0)
       }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "textItem", for: indexPath) as! textItemCellVC
        
        if indexPath == IndexPath(item: 0, section: 0) {
            cell.fieldName.text = "Title"
            cell.fieldValue.text = post?.title
        }
        
        if indexPath == IndexPath(item: 1, section: 0) {
            cell.fieldName.text = "Body"
            cell.fieldValue.text = post?.body
        }
        
        if indexPath == IndexPath(item: 2, section: 0) {
            cell.fieldName.text = "URL"
            cell.fieldValue.text = post?.url
        }
        
        
        if indexPath == IndexPath(item: 3, section: 0) {
            cell.fieldName.text = "Tags"
            cell.fieldValue.text = post?.tag
        }
        
        if indexPath == IndexPath(item: 4, section: 0) {
            cell.fieldName.text = "Author"
            cell.fieldValue.text = post?.author
        }
        
        return cell
    }
    
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        performSegue(withIdentifier: "editItemSegue", sender: indexPath)
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        if segue.identifier == "editItemSegue" {

            let indexPath = sender as! IndexPath
            let destVC = segue.destination as! editItemVC

            
            if indexPath == IndexPath(item: 0, section: 0) {
                destVC.itemTitle = "Title"
                destVC.itemValue = post?.title
                destVC.itemIndexPath = IndexPath(item: 0, section: 0)
            }
            
            if indexPath == IndexPath(item: 1, section: 0) {
                destVC.itemTitle = "Body"
                destVC.itemValue = post?.body
                destVC.itemIndexPath = IndexPath(item: 1, section: 0)
            }
            
            if indexPath == IndexPath(item: 2, section: 0) {
                destVC.itemTitle = "URL"
                destVC.itemValue = post?.url
                destVC.itemIndexPath = IndexPath(item: 2, section: 0)
            }
            
            
            if indexPath == IndexPath(item: 3, section: 0) {
                destVC.itemTitle = "Tags"
                destVC.itemValue = post?.tag
                destVC.itemIndexPath = IndexPath(item: 3, section: 0)
            }
            
            if indexPath == IndexPath(item: 4, section: 0) {
                destVC.itemTitle = "Author"
                destVC.itemValue = post?.author
                destVC.itemIndexPath = IndexPath(item: 4, section: 0)
            }
            
            
        }
    }
    
    func updatePost(refIndexPath:IndexPath, newValue:String) {
        
        if refIndexPath == IndexPath(item: 0, section: 0) {
            post?.title = newValue
        }
        
        if refIndexPath == IndexPath(item: 1, section: 0) {
            post?.body = newValue
        }
        
        if refIndexPath == IndexPath(item: 2, section: 0) {
            post?.url = newValue
        }
        
        if refIndexPath == IndexPath(item: 3, section: 0) {
            post?.tag = newValue
        }
        
        if refIndexPath == IndexPath(item: 4, section: 0) {
            post?.author = newValue
        }
        
        tableView.reloadData()
    }
    
    @IBAction func save(_ sender: Any) {
        
        let postPost = postPostRemoteController()
        
        if post?.body.count ?? 0 > 0 && post?.title.count ?? 0 > 0 {
        
            _ = postPost.post(post: post!, action: action!)
        } else {
            // Alert that required fields are missing
        }
    }
}
