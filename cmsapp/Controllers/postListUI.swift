//
//  postListUI.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-07.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit
import Down


class postListUI: UITableViewController {
    
    var postList = [Post]()
    var imageURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        updateTable()
        
        self.refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(postListUI.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(self.refreshControl!)
        
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! postCellVC
        
        
        
        let post = postList[indexPath.row]
        var postBody = post.body
        
        
        do {
            postBody = try Down(markdownString: post.body).toHTML()
            imageURL = postBody.getImageURL()
        } catch {}
        
        cell.postTitle.text = post.title
        cell.postBody.text = postBody.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        if imageURL.count > 0 { cell.postImage.imageFromURL(urlString: String(imageURL)) }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "viewPost", sender: indexPath)
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {

        if segue.identifier == "viewPost" {
                let indexPath = sender as! IndexPath
                let destVC = segue.destination as! postViewVC
                let postId = postList[indexPath.row].id
            destVC.postId = postId
        }
    }
    

    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        updateTable()
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func updateTable() {
        let PLRC = postListRemoteController()
        
        postList = PLRC.get()
    }
}





