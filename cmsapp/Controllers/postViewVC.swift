//
//  postViewVC.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-09.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import WebKit

class postViewVC: UIViewController, WKNavigationDelegate {
    

    @IBOutlet weak var postWebView: WKWebView!

    var postId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        postWebView.navigationDelegate = self
        
        let url = URL(string:global.appUrlDomain+"/p/\(String(describing: postId!))")
 
        let request = URLRequest(url:url!)
        postWebView.load(request)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        
        if segue.identifier == "editPostSegue" {
            let destVC = segue.destination as! postVC
            destVC.postId = postId
        }
    }
    
}
