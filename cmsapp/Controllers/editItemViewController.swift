//
//  editItemViewController.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-19.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit

class editItemVC:UIViewController {
    
    var itemTitle: String?
    var itemValue: String?
    var itemIndexPath: IndexPath?
    
    var parentVC: UIViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        titleLabel.text = itemTitle
        valueLabel.text = itemValue
        
        parentVC = self.navigationController?.viewControllers[(navigationController?.viewControllers.count)! - 2]
        
        
    }
    
 
    override func willMove(toParent parent: UIViewController?) {

        if let destinationVC = parentVC as? postVC {

            destinationVC.updatePost(refIndexPath:itemIndexPath!, newValue:valueLabel.text)
        }
    }
    
}
