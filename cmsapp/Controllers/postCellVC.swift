//
//  postCellVC.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-08.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit


class postCellVC: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    
    @IBOutlet weak var postBody: UILabel!
}
