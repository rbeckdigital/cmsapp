//
//  extensions.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-08.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit


extension UIImageView {
    public func imageFromURL(urlString: String) {
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                self.image = image
            })
            
        }).resume()
    }
}



extension String {
    
    public func getImageURL()->String {
        
        // Search for one string in another.
        let result = self.range(of: "src=",
                              options: NSString.CompareOptions.literal,
                              range: self.startIndex..<self.endIndex,
                              locale: nil)
        
        if let range = result {
            let separator = self[range.upperBound...range.upperBound].first
            
            let imageStart = self[self.index(range.upperBound, offsetBy:1)..<self.endIndex]

            let image = imageStart[imageStart.startIndex..<imageStart.firstIndex(of: separator ?? "'")!]
            
            return String(image)
        }
        
        return ""
    }
    
    
}
