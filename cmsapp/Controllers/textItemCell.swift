//
//  textItemCell.swift
//  cmsapp
//
//  Created by Richard Beck on 2018-12-19.
//  Copyright © 2018 Richard Beck. All rights reserved.
//

import Foundation
import UIKit


class textItemCellVC: UITableViewCell {
    

    @IBOutlet weak var fieldName: UILabel!
    
    @IBOutlet weak var fieldValue: UILabel!
}
